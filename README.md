# MiSoS(oup) Workflow Template

Cookiecutter template for MiSoS(oup) workflows with `snakemake`. This template
will generate a working directory with a `Snakefile` and `config.yaml`.

## Usage

```bash
pip install cookiecutter
cookiecutter git@gitlab.ethz.ch:ochsnern/misosoup_workflow_template.git
```

## Example

This example shows how to use MiSoS(oup) on multiple models on HPC Euler. To 
follow this example you are expected to have an active ssh connection with a
command prompt.

### Setting up the environment

To use `misosoup` and `snakemake` we will need to install a couple of tools.
Before we do so however, we want to load into a more recent version of python.

```bash
env2lmod
module load gcc/6.3.0
module load gurobi/9.0.2 python/3.7.4
```

Next `gurobi` needs to be setup. Follow these [instructions](https://scicomp.ethz.ch/wiki/Gurobi#Python_interface)
on the setup of the python interface.

Now we can install `misosoup` and its requirements.

```bash
git clone git@gitlab.ethz.ch:ochsnern/misosoup.git
cd misosoup
git submodule init
git submodule update
pip install --user libs/reframed
pip install --user .
```

The last tool that needs to be installed is `snakemake` and the environment that
we use for it. We use a tool called `cookiecutter` to create a bunch of
configuration files that we need for `snakemake` and `cookiecutter`.
`cookiecutter` will ask a couple of questions on how to set up the environment.
Make sure you set the memory unit to **MB** (on Euler), the memory limit to 4096
MB and increase the job limit, e.g. to 1000.

```bash
pip install --user snakemake cookiecutter
cookiecutter --output-dir "${HOME}/.config/snakemake" "gh:Snakemake-Profiles/lsf"
cookiecutter "git@gitlab.ethz.ch:ochsnern/misosoup_workflow_template.git"
```

Nice! The environment should be up and running.

### Preparing the data

Move into the workflow directory and check the `config.yaml` file.

```bash
cd miso_workflow
cat config.yaml
```

The output should be 

```
strains:
  - A1R12
  - I2R16
carbon_sources:
  - ac
base_medium:
  data/medium.yaml
models:
  data/models/*.xml
```

The first two lists (`strains` and `carbon_sources`) are the experiments that
should be evaluated. The other arguments lead to the files that store the base
medium and the models. Make sure the arguments point to the files (wherever they
are).

### Run the Job

```bash
bsub -W 120:00 -n 1 -o snake.out -J snake snakemake --profile lsf
```